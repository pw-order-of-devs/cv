FROM nginx:1.17-alpine
MAINTAINER Paweł Walus <pawel.walus@protonmail.com>

COPY dist /usr/share/nginx/html
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]
