import Vue from "vue"
import {margin, pageHeight} from "@/consts/page_size"

Vue.directive("split-page", {
  inserted: function (el) {
    const prev = el.previousElementSibling

    const rect = el.getBoundingClientRect()
    const top = rect.top + scrollY
    const bottom = rect.bottom + scrollY
    const pageNumber = Math.floor(bottom / pageHeight)
    const currentOffsetTop = pageHeight * pageNumber

    if (top < currentOffsetTop + margin && bottom > currentOffsetTop - margin) {
      const id = "page-breaker" + Math.floor(Math.random() * 100000)
      const style = document.createElement("style")
      style.setAttribute("media", "print")
      style.setAttribute("type", "text/css")

      if (prev && prev.classList.contains("field-header")) {
        const nMargin = currentOffsetTop - prev.getBoundingClientRect().top + margin * 2.4
        style.innerText = `#${id} { padding-top: ${ nMargin }px }`
        prev.setAttribute("id", id)
      } else {
        const nMargin = currentOffsetTop - top + margin * 3
        style.innerText = `#${id} { padding-top: ${ nMargin }px }`
        el.setAttribute("id", id)
      }
      document.head.appendChild(style)
    }
  }
})
