import Vue from "vue"

import { BootstrapVue, IconsPlugin } from "bootstrap-vue"
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.min.css"
import "bootstrap-xxs/bootstrap-xxs-tn.min.css"

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

import "@/directives/split_page"

import App from "./App.vue"

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount("#app")
