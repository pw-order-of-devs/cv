export const bootstrapMixin = {
  data: () => ({
    tableCol4: "col-tn-4 col-xxs-4 col-xs-4 col-sm-4 trimText",
    tableCol8: "col-tn-8 col-xxs-8 col-xs-8 col-sm-8 trimText",
  }),
}
