import * as jsyaml from "js-yaml"

export default {
  data: () => ({
    publicPath: process.env.BASE_URL,
    data: "",
  }),
  mounted () {
    const rawFile = new XMLHttpRequest();
    rawFile.open("GET", "data/data.yml", false);
    rawFile.onreadystatechange = () => {
      if(rawFile.readyState === 4 && (rawFile.status === 200 || rawFile.status === 0))
        this.data = jsyaml.load(rawFile.responseText)
    }
    rawFile.send(null)
  }
}
