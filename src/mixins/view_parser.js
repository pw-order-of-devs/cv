import * as consts from "@/consts/page_size";

export default {
  updated () {
    const count = Math.floor(document.body.scrollHeight / consts.pageHeight)
    document.getElementById("template").setAttribute("style", `height:${consts.pageHeight * (count + 1)}px;`)
  }
}
